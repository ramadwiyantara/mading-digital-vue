module.exports = {
  transpileDependencies: ["vuetify"],
  //   css: {
  //     loaderOptions: {
  //       sass: {
  //         implementation: require("sass"),
  //         additionalData: "@import '@/styles/main.scss'",
  //       },
  //       scss: {
  //         additionalData: "@import '@/styles/main.scss';",
  //       },
  //     },
  //   },
  module: {
    rules: [
      {
        test: /\.s(c|a)ss$/,
        use: [
          "vue-style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            // Requires sass-loader@^7.0.0
            options: {
              implementation: require("sass"),
              indentedSyntax: true, // optional
              data: "@import '@/styles/main.scss'",
            },
            // Requires >= sass-loader@^8.0.0
            options: {
              implementation: require("sass"),
              sassOptions: {
                indentedSyntax: true, // optional
              },
              prependData: "@import '@/styles/main.scss'",
            },
            options: {
              // This is the path to your variables
              additionalData: "@import '@/styles/main.scss'",
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          "vue-style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            // Requires sass-loader@^7.0.0
            options: {
              // This is the path to your variables
              data: "@import '@/styles/main.scss';",
            },
            // Requires sass-loader@^8.0.0
            options: {
              // This is the path to your variables
              prependData: "@import '@/styles/main.scss';",
            },
            // Requires sass-loader@^9.0.0
            options: {
              // This is the path to your variables
              additionalData: "@import '@/styles/main.scss';",
            },
          },
        ],
      },
    ],
  },
};
