import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import moment from "moment-timezone";
import VueMoment from "vue-moment";
import testMixins from "./mixins/testMixins";

Vue.mixin(testMixins);

Vue.use(VueMoment, {
  moment,
});
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
